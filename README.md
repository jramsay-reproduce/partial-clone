# partial-clone

## Clone (not working)

```bash
# Clone: --filter=sparse:oid
git clone --no-checkout --filter=sparse:oid=master:snazzy-app/.gitfilterspec <url>
```

## Fetch 

```bash
# Create local directory
mkdir huge-app
cd huge-app

# Setup
git init
git remote add origin <url>
git config --local extensions.partialClone origin

# Fetch: --filter=sparse:oid
git fetch --filter=sparse:oid=master:snazzy-app/.gitfilterspec origin
```

# Sparse checkout

```bash
# Enable sparse checkout
git config --local core.sparsecheckout true
    
# Configure sparse checkout
git show master:snazzy-app/.gitfilterspec >> .git/info/sparse-checkout

# Checkout master
git checkout master
```